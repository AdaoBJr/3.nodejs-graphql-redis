import mongoose from 'mongoose';

const Schema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

const Customer_Collection = 'customers';
export default mongoose.model(Customer_Collection, Schema);
