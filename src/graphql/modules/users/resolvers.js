import User from '../../../models/user';
import mongoose from 'mongoose';
import { getRedis, setRedis } from '../../../../redisConfig';

export default {
  User: {
    fullName: (user) => `${user.firstName} ${user.lastName}`,
  },
  Query: {
    users: async () => await User.find(),
    user: async (_, { id }) => {
      // console.time();
      const userRedis = await getRedis(`user_${id}`);

      if (userRedis) {
        const user = JSON.parse(userRedis);
        const objectId = mongoose.Types.ObjectId(user._id);
        user.id = objectId;
        delete user._id;
        // console.timeEnd();

        return user;
      }

      const userMongo = await User.findById(id);
      await setRedis(`user_${id}`, JSON.stringify(userMongo));
      // console.timeEnd();

      return userMongo;
    },
  },
  Mutation: {
    createUser: async (_, { data }) => await User.create(data),
    updateUser: async (_, { id, data }) =>
      await User.findOneAndUpdate(id, data, { new: true }),
    deleteUser: async (_, { id }) => !!(await User.findOneAndDelete(id)),
  },
};
