import useCustomer from '../../../magento/customer/useCustomer';
import {
  CUSTOMER_LOGIN_MUTATION,
  CUSTOMER_QUERY,
} from '../../../magento/customer/customer.gql';
import { getRedis, setRedis } from '../../../../redisConfig';

const { requestMagento } = useCustomer();

export default {
  Query: {
    customer: async (_, { data }) => {
      const { email, token } = data;

      const userRedis = await getRedis(`customer_${email}`);
      if (userRedis) return JSON.parse(userRedis);

      const customerMagento = await requestMagento(CUSTOMER_QUERY, null, token, 'obj');
      await setRedis(`customer_${email}`, JSON.stringify(customerMagento));

      return customerMagento;
    },
  },
  Mutation: {
    customerLogin: async (_, { data }) =>
      await requestMagento(CUSTOMER_LOGIN_MUTATION, data),
  },
};
