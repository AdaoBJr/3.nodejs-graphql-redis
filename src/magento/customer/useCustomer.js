import { GraphQLClient } from 'graphql-request';

export default function useCustomer() {
  const client = new GraphQLClient(process.env.MAGENTO_BACKEND_URL);

  const requestMagento = async (gql, data, token, typeResult) => {
    const variables = data;
    const headers = token && { authorization: `Bearer ${token}` };

    const response = await client.request(gql, variables, headers);
    const obj = Object.values(response)[0];
    const string = Object.values(obj)[0];
    const result =
      typeResult === 'obj' ? obj : !typeResult || typeResult === 'string' ? string : null;
    return result;
  };

  return {
    requestMagento,
  };
}
