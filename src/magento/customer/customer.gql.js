import { gql } from 'graphql-request';

export const CUSTOMER_LOGIN_MUTATION = gql`
  mutation customerLogin($email: String!, $password: String!) {
    generateCustomerToken(email: $email, password: $password) {
      token
    }
  }
`;

export const CUSTOMER_QUERY = gql`
  query customer {
    customer {
      firstname
      lastname
      email
      is_subscribed
      created_at
    }
  }
`;
